package xyz.daos.rabbit.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    @Value("${rabbitmq.queue.question}")
    private String questionQueue;

    @Value("${rabbitmq.queue.answer}")
    private String answerQueue;

    @Value("${rabbitmq.queue.invalid}")
    private String invalidQueue;

    @Value("${spring.rabbitmq.questionKey}")
    private String questionKey;

    @Value("${spring.rabbitmq.answerKey}")
    private String answerKey;

    @Value("${spring.rabbitmq.invalidKey}")
    private String invalidKey;

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Bean("question")
    public Queue question() {
        return new Queue(questionQueue);
    }

    @Bean("answer")
    public Queue answer() {
        return new Queue(answerQueue);
    }

    @Bean("invalid")
    public Queue invalid() {
        return new Queue(invalidQueue);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(exchange);
    }

    @Bean
    public Binding bindingQuestion(@Qualifier("question") Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(questionKey);
    }

    @Bean
    public Binding bindingAnswer(@Qualifier("answer") Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(answerKey);
    }

    @Bean
    public Binding bindingInvalidAnswer(@Qualifier("invalid") Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(invalidKey);
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}

