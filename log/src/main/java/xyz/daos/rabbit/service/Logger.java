package xyz.daos.rabbit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import xyz.daos.rabbit.entity.InvalidMessage;


@Component
@Slf4j
public class Logger {

    @RabbitListener(queues = "${rabbitmq.queue.invalid}")
    public void receiveMessage(InvalidMessage invalidMessage) {
        log.error("############ INVALID ANSWER ############");
        log.error("QUESTION ->  " + invalidMessage.getQuestion());
        log.error("ANSWER ->  " + invalidMessage.getAnswer());
        log.error("############----------------############");
    }
}
